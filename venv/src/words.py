import time
dictionary = {}


def creating_Dictionary(path):
    file = open(path, "r")
    global dictionary
    for words in file:
        stripped_word = words.strip()
        sorted_character_list = sorted(stripped_word)
        sorted_word = ''.join(map(str,sorted_character_list))
        if sorted_word in dictionary.keys():

            dictionary[sorted_word].append(stripped_word)

        else:
            dictionary.setdefault(sorted_word, [])
            dictionary[sorted_word].append(stripped_word)
    return dictionary


def Checking_valid_words(word,dictionary):
     # global dictionary
     print(dictionary)
     character_list = sorted(word)
     sorted_word = ''.join(map(str,character_list))
     if sorted_word in dictionary.keys():
       return dictionary[sorted_word]





if __name__ == "__main__":
    start = time.time()
    file_path = "C:/Users/SSubburam/Videos/python/dict.txt"
    dict = creating_Dictionary(file_path)
    input_word = input("Enter the word")
    valid_words = Checking_valid_words(input_word,dict)
    print(valid_words)

    print("execution-time----%s" %(time.time()-start))