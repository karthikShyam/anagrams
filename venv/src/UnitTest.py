import unittest


class AnagramTestCase(unittest.TestCase):
     def test_dictionary(self):
       from words import creating_Dictionary

       self.assertEqual(creating_Dictionary("C:/Users/SSubburam/Videos/python/dict.txt"),{'aahks': ['akash', 'akash'], 'aimt': ['amit', 'imta', 'mtai', 'tami'], 'ajnu': ['anuj'], 'ajnru': ['arjun'], 'aabhs': ['basha'], 'bbii': ['bibi'], 'bor': ['bro'], 'accht': ['catch'], 'cent': ['cent'], 'ccios': ['cisco'], 'corw': ['crow']})


     def test_valid_words(self):
         from words import Checking_valid_words
         self.assertEqual(Checking_valid_words("amit",{'aahks': ['akash', 'akash'], 'aimt': ['amit', 'imta', 'mtai', 'tami'], 'ajnu': ['anuj'], 'ajnru': ['arjun'], 'aabhs': ['basha'], 'bbii': ['bibi'], 'bor': ['bro'], 'accht': ['catch'], 'cent': ['cent'], 'ccios': ['cisco'], 'corw': ['crow']}),['amit', 'imta', 'mtai', 'tami'])
if __name__ == '__main__':
    unittest.main()
